# spring-authorization-server-bug-reproducer

This application is a minimal reproducer for a bug found on Spring Authorization Server.

To reproduce simply run
```
./gradlew bootRun
```