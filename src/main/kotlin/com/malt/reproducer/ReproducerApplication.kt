package com.malt.reproducer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReproducerApplication

fun main(args: Array<String>) {
	runApplication<ReproducerApplication>(*args)
}
